package com.mediceausum.android.user.presenter.ipresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface ITwilloVideoPresenter extends IPresenter {
    void getTwiloToken(Object object);
}
