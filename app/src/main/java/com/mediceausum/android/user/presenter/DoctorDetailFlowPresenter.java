package com.mediceausum.android.user.presenter;

import com.mediceausum.android.user.model.CustomException;
import com.mediceausum.android.user.model.ProviderModel;
import com.mediceausum.android.user.model.dto.common.Provider;
import com.mediceausum.android.user.model.dto.response.ProviderResponse;
import com.mediceausum.android.user.model.listener.IModelListener;
import com.mediceausum.android.user.presenter.ipresenter.IDoctorFlowPresenter;
import com.mediceausum.android.user.view.adapter.listener.IProviderRecyclerAdapter;
import com.mediceausum.android.user.view.iview.IDoctorDetailView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class DoctorDetailFlowPresenter extends BasePresenter<IDoctorDetailView> implements IDoctorFlowPresenter {

    public DoctorDetailFlowPresenter(IDoctorDetailView iView) {
        super(iView);
    }

    IProviderRecyclerAdapter iProviderRecyclerAdapter = new IProviderRecyclerAdapter() {
        @Override
        public void onClickItem(int pos, Provider data) {
            //Item Click function desc here
        }

        @Override
        public void onVideoCall(Provider data) {

        }
    };

    @Override
    public void getProviderList() {
        iView.showProgressbar();
        new ProviderModel(new IModelListener<ProviderResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProviderResponse response) {
                iView.dismissProgressbar();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<ProviderResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getProviderList();
    }

    @Override
    public void searchProvider(String searchKey) {

    }



    @Override
    public void makeVideoCall(Provider data) {

    }

    @Override
    public void sendRequestVideoCall(Integer serviceTypeId, Integer providerID, String date, String time, String broadcast) {

    }
}
