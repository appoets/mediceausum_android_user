package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.model.dto.response.Cards;
import com.mediceausum.android.user.presenter.ipresenter.IWalletPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IWalletView extends IView<IWalletPresenter> {
    void setUp();
    void onSuccess(Cards cards);
    void onAddCard();
    void onDeleteCard();
}
