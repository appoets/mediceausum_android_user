package com.mediceausum.android.user.model;

import com.mediceausum.android.user.common.Constants;
import com.mediceausum.android.user.model.dto.common.videocalldata.Fcm;
import com.mediceausum.android.user.model.dto.response.BaseResponse;
import com.mediceausum.android.user.model.listener.IModelListener;
import com.mediceausum.android.user.model.webservice.ApiClient;
import com.mediceausum.android.user.model.webservice.ApiInterface;

import java.util.List;

public class FCMModel extends BaseModel<BaseResponse> {

    public FCMModel(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<BaseResponse> response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void sendFCM(Fcm fcm){
        enQueueTask(new ApiClient().getClient(Constants.URL.FCM_URL).create(ApiInterface.class).sendFCM(fcm));
    }
}
