package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.model.dto.common.Provider;
import com.mediceausum.android.user.presenter.SpecialistFragmentPresenter;
import com.mediceausum.android.user.view.adapter.ProviderRecyclerAdapter;
import com.mediceausum.android.user.view.adapter.ServiceRecyclerAdapter;

/**
 * Created by Tranxit Technologies.
 */

public interface ISpecialistView extends IView<SpecialistFragmentPresenter> {
    void setAdapter(ServiceRecyclerAdapter adapter);
    void setProviderAdapter(ProviderRecyclerAdapter adapter, Integer specialistID);
    void showVideoAlert(Provider data);
    void makeVideoCall(Provider data);
    void moveToDetailView(Provider data);
    void startCheckStatus(String request_id);
}
