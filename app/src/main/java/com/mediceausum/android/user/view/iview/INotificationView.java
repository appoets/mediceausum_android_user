package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.INotificationPresenter;
import com.mediceausum.android.user.view.adapter.NotificationRecyclerAdapter;

public interface INotificationView extends IView<INotificationPresenter> {
    void initSetUp();
    void setAdapter(NotificationRecyclerAdapter adapter);
}
