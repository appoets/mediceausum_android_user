package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.IMakeCallPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IMakeCallView extends IView<IMakeCallPresenter> {
        void setUp();
}
