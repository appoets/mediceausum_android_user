package com.mediceausum.android.user.presenter.ipresenter;

public interface  IOnBoardPresenter extends IPresenter {

    void goToLogin();
}
