package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.IEditProfilePresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IEditProfileView extends IView<IEditProfilePresenter> {
}
