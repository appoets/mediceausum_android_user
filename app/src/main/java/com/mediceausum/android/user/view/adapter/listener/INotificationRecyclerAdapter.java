package com.mediceausum.android.user.view.adapter.listener;

import com.mediceausum.android.user.model.dto.response.NotificationResponse;

/**
 * Created by Tranxit Technologies.
 */

public interface INotificationRecyclerAdapter extends BaseRecyclerListener<NotificationResponse> {
}
