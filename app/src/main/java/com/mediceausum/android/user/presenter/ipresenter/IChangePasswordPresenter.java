package com.mediceausum.android.user.presenter.ipresenter;

import com.mediceausum.android.user.model.dto.request.ChangePasswordRequest;

public interface IChangePasswordPresenter extends IPresenter {
    void goToLogin();
    void changePassword(ChangePasswordRequest request);
}