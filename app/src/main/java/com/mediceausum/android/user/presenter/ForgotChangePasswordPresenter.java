package com.mediceausum.android.user.presenter;

import com.mediceausum.android.user.presenter.ipresenter.IForgotChangePasswordPresenter;
import com.mediceausum.android.user.view.iview.IForgotChangePasswordView;


public class ForgotChangePasswordPresenter extends BasePresenter<IForgotChangePasswordView> implements IForgotChangePasswordPresenter {

    public ForgotChangePasswordPresenter(IForgotChangePasswordView iView) {
        super(iView);
    }

    @Override
    public void goToLogin() {
        iView.goToLogin();
    }
}
