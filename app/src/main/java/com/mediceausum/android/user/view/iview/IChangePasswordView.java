package com.mediceausum.android.user.view.iview;


import com.mediceausum.android.user.presenter.ipresenter.IChangePasswordPresenter;

public interface IChangePasswordView extends IView<IChangePasswordPresenter> {
                void goToLogin();
}
