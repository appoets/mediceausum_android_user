package com.mediceausum.android.user.view.iview;

import android.os.Bundle;

import com.mediceausum.android.user.model.dto.response.invoice.Invoice;
import com.mediceausum.android.user.presenter.ipresenter.IInvoicePresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IInvoiceView extends IView<IInvoicePresenter> {
        void setUp(Bundle bundle);
        void updateInvoiceData(Invoice data);
        void paymentSuccess();
}
