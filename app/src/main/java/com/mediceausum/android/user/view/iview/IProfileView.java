package com.mediceausum.android.user.view.iview;



import com.mediceausum.android.user.model.dto.response.ProfileResponse;
import com.mediceausum.android.user.presenter.ipresenter.IProfilePresenter;

public interface IProfileView extends IView<IProfilePresenter> {
    void updateUserDetails(ProfileResponse response);
}
