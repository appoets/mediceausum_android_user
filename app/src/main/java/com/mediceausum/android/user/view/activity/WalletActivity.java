package com.mediceausum.android.user.view.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cooltechworks.creditcarddesign.CardEditActivity;
import com.cooltechworks.creditcarddesign.CreditCardUtils;
import com.mediceausum.android.user.R;
import com.mediceausum.android.user.model.dto.response.Cards;
import com.mediceausum.android.user.presenter.WalletPresenter;
import com.mediceausum.android.user.presenter.ipresenter.IWalletPresenter;
import com.mediceausum.android.user.view.iview.IWalletView;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import butterknife.BindView;
import butterknife.OnClick;

public class WalletActivity extends BaseActivity<IWalletPresenter> implements IWalletView {

    final int GET_NEW_CARD = 2;
    @BindView(R.id.tvCardView)
    TextView tvCardView;
    @BindView(R.id.btnAddCard)
    Button btnAddCard;
    boolean back = false;
    private String mCardId = "";
    private String defaultCard = "XXXXXXXXXXXXXXXX";

    private String cardNumber = "";

    @Override
    int attachLayout() {
        return R.layout.activity_card;
    }

    @Override
    IWalletPresenter initialize() {
        return new WalletPresenter(this);
    }


    @Override
    public void setUp() {
        btnAddCard.setTextColor(getResources().getColor(R.color.colorBlack));
        btnAddCard.setText(R.string.add_card);
        iPresenter.getCard();
    }

    @OnClick({R.id.btnAddCard, R.id.ibBack})
    public void OnViewClick(View view) {
        switch (view.getId()) {

            case R.id.btnAddCard:
                if (btnAddCard.getText().toString().equalsIgnoreCase(getString(R.string.add_card))) {
                    Intent intent = new Intent(WalletActivity.this, CardEditActivity.class);
                    startActivityForResult(intent, GET_NEW_CARD);
                } else {
                    showAlertDialogWithNegativeButton(getString(R.string.delete_card_message), new DialogClick() {
                        @Override
                        public void positiveButtonClick() {
                            iPresenter.deleteCard(mCardId);
                        }

                        @Override
                        public void negativeButtonClick() {

                        }
                    });
                }

                break;

            case R.id.ibBack:
                onBackPressed();
                break;

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            showProgressbar();

            String cardHolderName = data.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME);
            cardNumber = data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER);
            String expiry = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY);
            String cvv = data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV);

            // Your processing goes here.
            String[] temp = expiry.split("/");

            int month = Integer.parseInt(temp[0]);
            int year = Integer.parseInt(temp[1]);


            Card card = new Card(
                    cardNumber,
                    month,
                    year,
                    cvv
            );

            if (card.validateNumber() && card.validateCVC()) {
                //showSnackBar("Card Added Successfully");
                Stripe stripe = new Stripe(this, "pk_test_pAqnif8mJ1kwh16JXuApB3Y5");
                stripe.createToken(
                        card,
                        new TokenCallback() {
                            public void onSuccess(Token token) {
                                // Send token to your server
                                dismissProgressbar();
                                iPresenter.addCard(token.getId());
                            }

                            public void onError(Exception error) {
                                // Show localized error message
                                dismissProgressbar();
                                showSnackBar(error.getLocalizedMessage());
                            }
                        }
                );
            } else {
                dismissProgressbar();
                showSnackBar("Invalid card details");
            }

        }
    }

    @Override
    public void onSuccess(Cards cards) {
        if (cards != null) {
            String cardNo = defaultCard.substring(0, defaultCard.length() - 4) + cards.getLastFour();
            tvCardView.setText(cardNo);
            btnAddCard.setTextColor(getResources().getColor(R.color.colorMaterialRed300));
            btnAddCard.setText(R.string.remove_card);
            mCardId = cards.getCardId();
        }

    }

    @Override
    public void onAddCard() {
        iPresenter.getCard();
    }

    @Override
    public void onDeleteCard() {
        btnAddCard.setTextColor(getResources().getColor(R.color.colorMaterialIndigo300));
        btnAddCard.setText(R.string.add_card);
        String cardNo = formatCreditCard(defaultCard);
        tvCardView.setText(cardNo);
    }

    private String formatCreditCard(String input) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            if (i % 4 == 0 && i != 0) {
                result.append("-");
            }

            result.append(input.charAt(i));
        }

        return result.toString();

    }
}
