package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.IForgotPasswordPresenter;

public interface IForgotPasswordView extends IView<IForgotPasswordPresenter> {
        void goToOneTimePassword();
}
