package com.mediceausum.android.user.view.adapter.listener;

import com.mediceausum.android.user.model.dto.common.Services;

/**
 * Created by Tranxit Technologies.
 */

public interface IServiceRecyclerAdapter extends BaseRecyclerListener<Services> {
}
