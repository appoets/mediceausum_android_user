package com.mediceausum.android.user.presenter;

import android.os.Bundle;

import com.mediceausum.android.user.presenter.ipresenter.IOnBoardPresenter;
import com.mediceausum.android.user.view.iview.IOnBoardView;

public class OnBoardPresenter extends BasePresenter<IOnBoardView> implements IOnBoardPresenter {

    public OnBoardPresenter(IOnBoardView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
    }

    @Override
    public void goToLogin() {
        iView.gotoLogin();
    }
}