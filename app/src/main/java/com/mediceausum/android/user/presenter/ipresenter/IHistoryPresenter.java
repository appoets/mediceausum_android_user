package com.mediceausum.android.user.presenter.ipresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IHistoryPresenter extends IPresenter {
    void getHistoryList();
}
